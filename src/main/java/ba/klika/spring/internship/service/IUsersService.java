package ba.klika.spring.internship.service;

import ba.klika.spring.internship.domain.User;

import java.util.List;

/**
 * @author nhrnjic, Klika doo, Sarajevo
 * created on 05.10.18.
 */

public interface IUsersService {
    List<User> getAllUsers(String owner, String repo);
}
