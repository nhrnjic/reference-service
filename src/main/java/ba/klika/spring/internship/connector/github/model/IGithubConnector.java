package ba.klika.spring.internship.connector.github.model;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "github-api",
        url = "${github.basePath}"
)
public interface IGithubConnector {
    @RequestMapping(
            value = "/repos/{owner}/{repo}/contributors",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    List<Contributor> getContributors(
            @PathVariable(name = "owner") String owner,
            @PathVariable(name = "repo") String repo
    );
}
