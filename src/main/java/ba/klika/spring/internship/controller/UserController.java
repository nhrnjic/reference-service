package ba.klika.spring.internship.controller;

import ba.klika.spring.internship.service.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author nhrnjic, Klika doo, Sarajevo
 * created on 05.10.18.
 */

@Controller
@RequestMapping(value = "users")
public class UserController {
    @Autowired
    private IUsersService usersService;

    @GetMapping("/index")
    public String getUsers(Model model){
        model.addAttribute("currentUserName", "Current User");
        model.addAttribute("users", usersService.getAllUsers("webpack", "webpack"));
        return "users/index";
    }

    @GetMapping("/show")
    public String getUser(Model model){
        return "users/show";
    }
}
