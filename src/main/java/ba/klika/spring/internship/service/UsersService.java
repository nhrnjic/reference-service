package ba.klika.spring.internship.service;

import ba.klika.spring.internship.domain.User;
import ba.klika.spring.internship.service.github.GithubConnectorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author nhrnjic, Klika doo, Sarajevo
 * created on 05.10.18.
 */

@Component
public class UsersService implements IUsersService {
    @Autowired
    private GithubConnectorService githubConnectorService;

    @Override
    public List<User> getAllUsers(String owner, String repo) {
        return githubConnectorService.getContributors(owner, repo);
    }
}
