package ba.klika.spring.internship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ReferenceServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReferenceServiceApplication.class, args);
	}
}
