package ba.klika.spring.internship.service.github;

import ba.klika.spring.internship.connector.github.model.Contributor;
import ba.klika.spring.internship.domain.User;

public final class ContributorMapper {
    private ContributorMapper(){};

    public static User fromRequest(Contributor contributor){
        User user = new User();
        user.setUsername(contributor.getLogin());
        return user;
    }
}
