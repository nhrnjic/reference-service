package ba.klika.spring.internship.connector.github.model;

/**
 * @author nhrnjic, Klika doo, Sarajevo
 * created on 05.10.18.
 */

public class Contributor {
    private String login;
    private int contributions;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getContributions() {
        return contributions;
    }

    public void setContributions(int contributions) {
        this.contributions = contributions;
    }
}
