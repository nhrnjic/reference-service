package com.klika.spring.internship.service;

import ba.klika.spring.internship.service.IUsersService;
import ba.klika.spring.internship.service.UsersService;
import ba.klika.spring.internship.service.github.GithubConnectorService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;


@RunWith(SpringRunner.class)
public class UsersServiceTest {
    @Autowired
    private IUsersService sut;

    @MockBean
    private GithubConnectorService githubConnectorService;

    @Test
    public void callGithubService_success(){
        //Given
        String owner = "webpack";
        String repo = "webpack";

        //When
        sut.getAllUsers(owner, repo);

        //Then
        verify(githubConnectorService,times(1))
                .getContributors(eq(owner), eq(repo));

        verifyNoMoreInteractions(githubConnectorService);
    }

    @TestConfiguration
    static class TestConfig{
        @Bean
        public IUsersService getSut(){
            return new UsersService();
        }
    }
}
