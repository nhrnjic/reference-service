package ba.klika.spring.internship.service.github;

import ba.klika.spring.internship.connector.github.model.IGithubConnector;
import ba.klika.spring.internship.domain.User;
import ba.klika.spring.internship.exception.BadRequestException;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GithubConnectorService {
    @Autowired
    private IGithubConnector githubConnector;

    public List<User> getContributors(String owner, String repo){
        try{
            return githubConnector.getContributors(owner, repo)
                    .stream()
                    .map(ContributorMapper::fromRequest)
                    .collect(Collectors.toList());
        }catch (FeignException e){
            switch (e.status()){
                case 400:
                    throw new BadRequestException("Bad request during call with params: " +
                            owner + " and " + repo);
                case 404: // handle 404
                    break;
                case 500: // handle 500
                    break;
            }
        }

        return null;
    }
}
